package com.ema.daa.orange.spain.sendsms.jms;

import org.slf4j.LoggerFactory;

import com.ema.process.IProcessElement;
import com.ema.process.IProcessElementGetter;

public abstract class AbstractMessageListener<M, D> {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AbstractMessageListener.class);
    private String processElementKey;
    private IProcessElementGetter processElementGetter;

    public final void setProcessElementKey(String processElementKey) {
        this.processElementKey = processElementKey;
    }

    public final void setProcessElementGetter(IProcessElementGetter processElementGetter) {
        this.processElementGetter = processElementGetter;
    }

    public final void onMessage(final M msg) throws Exception {
        try {
            D processObject = getProcessObject(msg);
            IProcessElement targetWorkflow = processElementGetter.getProcessElement(processElementKey);
            targetWorkflow.execute(processObject);
        } catch (Exception e) {
            LOGGER.error("Error handling JMS message", e);
            throw e;
        }
    }

    protected abstract D getProcessObject(M msg) throws Exception;

}
