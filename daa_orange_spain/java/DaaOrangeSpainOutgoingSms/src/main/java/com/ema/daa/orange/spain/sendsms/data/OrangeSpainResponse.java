package com.ema.daa.orange.spain.sendsms.data;

public class OrangeSpainResponse {
    private String errorCode;
    private String errorDescription;
    private String transactionId;

    public final String getTransactionId() {
        return transactionId;
    }

    public final void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public final String getErrorCode() {
        return errorCode;
    }

    public final void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public final String getErrorDescription() {
        return errorDescription;
    }

    public final void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
