package com.ema.daa.orange.spain.sendsms.data;

public class SendSmsDataIn {
    private Long daaBillingTransactionId;

    private String mobileIdentityNumber;
    private String daaOfferId;
    private String templateId;
    private String preferredLanguage;
    private String smsTimestamp;

    private int isModeTestActivate = 0;

    public final int getIsModeTestActivate() {
        return isModeTestActivate;
    }

    public final void setIsModeTestActivate(int isModeTestActivate) {
        this.isModeTestActivate = isModeTestActivate;
    }

    public final Long getDaaBillingTransactionId() {
        return daaBillingTransactionId;
    }

    public final void setDaaBillingTransactionId(Long daaBillingTransactionId) {
        this.daaBillingTransactionId = daaBillingTransactionId;
    }

    public final String getMobileIdentityNumber() {
        return mobileIdentityNumber;
    }

    public final void setMobileIdentityNumber(String mobileIdentityNumber) {
        this.mobileIdentityNumber = mobileIdentityNumber;
    }

    public final String getDaaOfferId() {
        return daaOfferId;
    }

    public final void setDaaOfferId(String daaOfferId) {
        this.daaOfferId = daaOfferId;
    }

    public final String getTemplateId() {
        return templateId;
    }

    public final void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public final String getPreferredLanguage() {
        return preferredLanguage;
    }

    public final void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public final String getSmsTimestamp() {
        return smsTimestamp;
    }

    public final void setSmsTimestamp(String smsTimestamp) {
        this.smsTimestamp = smsTimestamp;
    }

}
