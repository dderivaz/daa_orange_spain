package com.ema.daa.orange.spain.sendsms.jms;

public final class DaaNotification {

    private Long notificationId;

    public DaaNotification() {
    }

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    @Override
    public String toString() {
        return "DAANotification{" + "notificationId=" + notificationId + '}';
    }
}
