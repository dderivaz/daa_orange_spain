package com.ema.daa.orange.spain.sendsms.tasks;

import java.net.HttpURLConnection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ema.daa.orange.spain.sendsms.data.SendSmsData;
import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.process.IProcessElement;

public class ManageSendSmsAnswer implements IProcessElement<SendSmsData> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageSendSmsAnswer.class);
    private static final int ERROR_RETURN_CODE = -1;

    private List<String> successCodes;
    private List<String> errorCodes;

    public final void setErrorCodes(List<String> anErrorCodeList) {
        this.errorCodes = anErrorCodeList;
    }

    public final void setSuccessCodes(List<String> successCodes) {
        this.successCodes = successCodes;
    }

    @Override
    public final void execute(SendSmsData data) throws Exception {
        workflow(data); // simple way to force result return
    }

    private SendSmsData workflow(SendSmsData data) {
        LOGGER.debug("Computing result for Sendsms http call with ResponseCode: <{}> ",
                data.getServiceResponse().getHttpReturnCode());

        if (!(httpSuccessTechnical(data.getServiceResponse().getHttpReturnCode()))) { // Call was technically in error
            return technicalError(data);
        }

        if (!httpSuccessBusiness(data.getServiceOutputData().getErrorCode())) { // Call was incorrect from business
                                                                                // point of view
            return businessError(data);
        }

        return success(data);
    }

    private SendSmsData technicalError(SendSmsData data) {
        Integer httpReturnCode = data.getServiceResponse().getHttpReturnCode();
        data.getOutputData().setReturnStatus(BillingOutput.STATUS_UKN);
        if (httpReturnCode != null) {
            data.getOutputData().setReturnCode(httpReturnCode);
        }
        LOGGER.info("SendSms Request is considered as a technical failure as httpReturnCode is <{}>", httpReturnCode);
        return data;
    }

    private SendSmsData businessError(SendSmsData data) {
        String returnCode = data.getServiceOutputData().getErrorCode();

        data.getOutputData().setReturnCode(toErrorCode(returnCode));
        data.getOutputData().setReturnStatus(toReturnStatus(returnCode));
        LOGGER.info("SendSms call failed. Error Description <{}>, return code: <{}>, return status : <{}>",
                data.getServiceOutputData().getErrorDescription(), data.getServiceOutputData().getErrorCode(),
                data.getOutputData().getReturnStatus());
        return data;
    }

    private SendSmsData success(SendSmsData data) {
        data.getOutputData().setReturnStatus(BillingOutput.STATUS_OK);
        LOGGER.info("SendSms Success. Response status: <{}>", data.getServiceOutputData().getErrorCode());
        return data;
    }

    private int toReturnStatus(String returnCode) {
        if (errorCodes.contains(returnCode)) {
            return BillingOutput.STATUS_KO;
        }
        return BillingOutput.STATUS_UKN;
    }

    private int toErrorCode(String returnCode) {
        try {
            return Integer.parseInt(returnCode);
        } catch (NumberFormatException ex) {
            LOGGER.warn("Could not parse error code <{}> to integer.", returnCode);
            return ERROR_RETURN_CODE;
        }
    }

    private boolean httpSuccessTechnical(Integer httpReturnCode) {
        return httpReturnCode != null && HttpURLConnection.HTTP_OK == httpReturnCode;
    }

    private boolean httpSuccessBusiness(String returnCode) {
        return successCodes.contains(returnCode);
    }

}
