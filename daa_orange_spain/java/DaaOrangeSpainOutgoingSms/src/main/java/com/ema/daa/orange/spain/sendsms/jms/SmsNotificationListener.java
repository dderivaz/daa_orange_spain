package com.ema.daa.orange.spain.sendsms.jms;

import org.slf4j.LoggerFactory;

import com.ema.daa.orange.spain.sendsms.data.SendSmsData;
import com.ema.daa.orange.spain.sendsms.data.SendSmsDataIn;
import com.ema.daa.util.billing.output.BillingOutput;

public final class SmsNotificationListener extends AbstractMessageListener<DaaNotification, SendSmsData> {
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SmsNotificationListener.class);

    @Override
    protected SendSmsData getProcessObject(DaaNotification msg) throws Exception {
        LOGGER.error(">>>>>>>>>>>>>> JMS Message Received <<<<<<<<<<<<<<");
        LOGGER.error("Notification id is: ", msg.getNotificationId());
        LOGGER.error(msg.toString());
        SendSmsData smsData = new SendSmsData();
        smsData.setDaaNotification(msg);
        smsData.setInputData(new SendSmsDataIn());
        smsData.setOutputData(new BillingOutput());
        return smsData;
    }

}
