package com.ema.daa.orangespain.billing.data;

import java.util.HashMap;
import java.util.Map;

import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.process.component.http.beans.HttpResponseDefinition;
import com.ema.process.service.IServiceData;

public final class BillingData implements IServiceData<BillingDataIn, BillingOutput> {

    private BillingDataIn inputData;
    private BillingOutput outputData = new BillingOutput();
    private String serviceRequest;
    private HttpResponseDefinition serviceResponse;
    private BillingServiceResponse billingServiceResponse = new BillingServiceResponse();

    private Map<String, Object> otherData = new HashMap<String, Object>();

    public BillingDataIn getInputData() {
        return inputData;
    }

    @Override
    public void setInputData(BillingDataIn inputData) {
        this.inputData = inputData;
    }

    @Override
    public BillingOutput getOutputData() {
        return outputData;
    }

    public void setOutputData(BillingOutput outputData) {
        this.outputData = outputData;
    }

    public String getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(String serviceRequest) {
        this.serviceRequest = serviceRequest;
    }

    public HttpResponseDefinition getServiceResponse() {
        return serviceResponse;
    }

    public void setServiceResponse(HttpResponseDefinition serviceResponse) {
        this.serviceResponse = serviceResponse;
    }

    public BillingServiceResponse getBillingServiceResponse() {
        return billingServiceResponse;
    }

    public void setBillingServiceResponse(BillingServiceResponse billingServiceResponse) {
        this.billingServiceResponse = billingServiceResponse;
    }

    public Map<String, Object> getOtherData() {
        return otherData;
    }

    public void setOtherData(Map<String, Object> otherData) {
        this.otherData = otherData;
    }

}
