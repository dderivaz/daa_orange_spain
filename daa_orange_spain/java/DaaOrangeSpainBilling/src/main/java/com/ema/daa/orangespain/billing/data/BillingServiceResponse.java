package com.ema.daa.orangespain.billing.data;

public class BillingServiceResponse {

    private String errorCode;
    private String errorDescription;

    public final String getErrorCode() {
        return errorCode;
    }

    public final void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public final String getErrorDescription() {
        return errorDescription;
    }

    public final void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
