package com.ema.daa.orangespain.billing.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import com.ema.daa.orangespain.billing.data.BillingData;
import com.ema.daa.orangespain.billing.data.BillingDataIn;
import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.daa.util.billing.output.BillingOutputLegacy;
import com.ema.daa.util.billing.output.adapters.LegacyAdapter;
import com.ema.process.webservice.WebServiceImpl;

@WebService
public final class BillingEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(BillingEndpoint.class);
    private WebServiceImpl<BillingDataIn, BillingOutput, BillingData> internalServiceSubscribe;

    @WebMethod(exclude = true)
    public void setInternalServiceSubscribe(WebServiceImpl<BillingDataIn, BillingOutput, BillingData> internalService) {
        this.internalServiceSubscribe = internalService;
    }

    @WebMethod(operationName = "billingService")
    @WebResult(name = "billingDataOut")
    @XmlJavaTypeAdapter(LegacyAdapter.class)
    public BillingOutputLegacy executeBilling(@WebParam(name = "billingDataIn") BillingDataIn inputData)
            throws InstantiationException, DataAccessException, IllegalAccessException, Exception {
        LOGGER.debug(" Executing billing operation for daa billing transaction id : <{}> ",
                inputData.getDaaBillingTransactionId());
        try {
            BillingOutput output = internalServiceSubscribe.execute(inputData);
            return new BillingOutputLegacy(output);
        } catch (Exception e) {
            LOGGER.warn("Exception occurs while executing billing interface", e);
            return new BillingOutputLegacy(BillingOutput.DEFAULT_UKN_ERROR);
        }

    }
}
