package com.ema.daa.orangespain.billing.workflow;

import java.net.HttpURLConnection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ema.daa.orangespain.billing.data.BillingData;
import com.ema.daa.util.billing.output.BillingOutput;
import com.ema.process.IProcessElement;

public class ManageBillingAnswer implements IProcessElement<BillingData> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManageBillingAnswer.class);
    private static final int ERROR_RETURN_CODE = -1;

    private List<String> successCodes;
    private List<String> errorCodes;

    public final void setErrorCodes(List<String> anErrorCodeList) {
        this.errorCodes = anErrorCodeList;
    }

    public final void setSuccessCodes(List<String> successCodes) {
        this.successCodes = successCodes;
    }

    @Override
    public final void execute(BillingData data) throws Exception {
        workflow(data); // simple way to force result return
    }

    private BillingData workflow(BillingData data) {
        LOGGER.debug("Computing result for Billing http call with ResponseCode: <{}> ",
                data.getServiceResponse().getHttpReturnCode());

        if (!(httpSuccessTechnical(data.getServiceResponse().getHttpReturnCode()))) { // Call was technically in error
            return technicalError(data);
        }

        if (!httpSuccessBusiness(data.getBillingServiceResponse().getErrorCode())) { // Call was incorrect from business
                                                                                     // point of view
            return businessError(data);
        }

        return success(data);
    }

    private BillingData technicalError(BillingData data) {
        Integer httpReturnCode = data.getServiceResponse().getHttpReturnCode();
        data.getOutputData().setReturnStatus(BillingOutput.STATUS_UKN);
        if (httpReturnCode != null) {
            data.getOutputData().setReturnCode(httpReturnCode);
        }
        LOGGER.info("Billing Request is considered as a technical failure as httpReturnCode is <{}>", httpReturnCode);
        return data;
    }

    private BillingData businessError(BillingData data) {
        String returnCode = data.getBillingServiceResponse().getErrorCode();

        data.getOutputData().setReturnCode(toErrorCode(returnCode));
        data.getOutputData().setReturnStatus(toReturnStatus(returnCode));
        LOGGER.info("Billing call failed. Error Description <{}>, return code: <{}>, return status : <{}>",
                data.getBillingServiceResponse().getErrorDescription(), data.getBillingServiceResponse().getErrorCode(),
                data.getOutputData().getReturnStatus());
        return data;
    }

    private BillingData success(BillingData data) {
        data.getOutputData().setReturnStatus(BillingOutput.STATUS_OK);
        LOGGER.info("Billing Success. Response status: <{}>", data.getBillingServiceResponse().getErrorCode());
        return data;
    }

    private int toReturnStatus(String returnCode) {
        if (errorCodes.contains(returnCode)) {
            return BillingOutput.STATUS_KO;
        }
        return BillingOutput.STATUS_UKN;
    }

    private int toErrorCode(String returnCode) {
        try {
            return Integer.parseInt(returnCode);
        } catch (NumberFormatException ex) {
            LOGGER.warn("Could not parse error code <{}> to integer.", returnCode);
            return ERROR_RETURN_CODE;
        }
    }

    private boolean httpSuccessTechnical(Integer httpReturnCode) {
        return httpReturnCode != null && HttpURLConnection.HTTP_OK == httpReturnCode;
    }

    private boolean httpSuccessBusiness(String returnCode) {
        return successCodes.contains(returnCode);
    }

}
