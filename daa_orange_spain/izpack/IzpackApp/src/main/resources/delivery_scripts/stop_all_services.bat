@ECHO OFF

echo *********************************
echo * STOPPING ALL Tomcat SERVICES ...
echo *********************************
net stop ema_cores
net stop ema_files
net stop ema_inbound
net stop ema_inbound_web
net stop ema_monitoring
net stop ema_outbound
echo STOPPING ALL DAA SERVICES [Done]

echo *********************************
echo * STOP ALL DAA SERVICES ...
echo *********************************
net stop daa_core_connectivity
net stop daa_core_connectivity_debit
