@echo off
set OUG_ENV=${INSTALL_PATH}\env_config\daa_connectivity
set IB_HOME=${INSTALL_PATH}\deliveries\connectivity_binaries-${com.ema.connecitivity.version}
set OUG_HOME=${INSTALL_PATH}\deliveries\connectivity_binaries-${com.ema.connecitivity.version}\wrapper

echo Connectivity Install path is : %IB_HOME%
echo Connectivity Environment path is : %OUG_ENV%
cd %OUG_ENV%

"%OUG_HOME%\wrapper.exe" "%OUG_ENV%/wrapper.conf"

pause